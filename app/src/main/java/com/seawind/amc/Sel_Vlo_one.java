package com.seawind.amc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Sel_Vlo_one extends AppCompatActivity {

    EditText name, phone, email, pin, add;
    FloatingActionButton fabNext;
    ProgressDialog progressDialog;
    public static String name_s, phone_s, email_s, pin_s, add_s, spring_s;
    Spinner sp_ward_number;
    ArrayList<String> arrayList = new ArrayList<>();
    public static Sel_Vlo_one sel_vlo_one;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sel_vlo);
        sel_vlo_one = this;

        progressDialog = ProgressDialog.show(Sel_Vlo_one.this, null, null, false, true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.layout_loading_dialog);
        progressDialog.setCancelable(false);
        progressDialog.dismiss();

        name = findViewById(R.id.ed_name);
        phone = findViewById(R.id.ed_phone);
        email = findViewById(R.id.ed_email);
        pin = findViewById(R.id.ed_pin);
        add = findViewById(R.id.ed_add);
        fabNext = findViewById(R.id.fabNext);
        sp_ward_number = (Spinner) findViewById(R.id.sp_ward_number);

        try {
            Url.getWebService().getMasterDetails(0, 1).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        progressDialog.dismiss();
                    } catch (Exception e1) {

                    }
                    if (response.isSuccessful()) {
                        try {
                            JSONObject responseObject = AppConstant.getResponseObject(response);

                            if (responseObject.optBoolean("IsSuccess")) {
                                String detail = responseObject.optString("ResponseData");
                                try {
                                    List<Grid_data> temp_survey = new Gson().fromJson(responseObject
                                            .optString("ResponseData"), new TypeToken<List<Grid_data>>() {
                                    }.getType());

                                    try {
                                        for (int i = 0; i < temp_survey.size(); i++) {
                                            arrayList.add(temp_survey.get(i).getName());
                                        }
                                    } catch (Exception e) {

                                    }

                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(Sel_Vlo_one.this, android.R.layout.simple_spinner_dropdown_item, arrayList);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                    sp_ward_number.setAdapter(adapter);

                                } catch (Exception e) {

                                }
                            } else {
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            Toast.makeText(Sel_Vlo_one.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("", "Error in Ticket Category : " + t.getMessage());
                }
            });

        } catch (Exception e) {

        }
        fabNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    name_s = name.getText().toString();
                    phone_s = phone.getText().toString();
                    email_s = email.getText().toString();
                    pin_s = pin.getText().toString();
                    add_s = add.getText().toString();
                    spring_s = (sp_ward_number.getSelectedItem().toString());

                    if (!name_s.isEmpty()) {

                        if (phone_s.length() == 10 && Patterns.PHONE.matcher(phone_s).matches()) {

                            if (!email_s.equals("") && Patterns.EMAIL_ADDRESS.matcher(email_s).matches()) {

                                Intent intent = new Intent(Sel_Vlo_one.this, Sel_Vol_sec.class);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(Sel_Vlo_one.this, Sel_Vol_sec.class);
                                startActivity(intent);
                            }
                        } else {
                            phone.setError("Invalid");
                        }
                    } else {
                        name.setError("Invalid");
                    }
                } catch (Exception e) {

                }
            }
        });
    }
}
