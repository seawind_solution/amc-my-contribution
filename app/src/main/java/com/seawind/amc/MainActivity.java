package com.seawind.amc;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.transition.TransitionManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    TextView textView;
    boolean mSecondText;
    RelativeLayout transitionsContainer;
    public static final String TEXT_1 = "Maru Yogdan";
    public static final String TEXT_2 = "મારુ યોગદાન";
    private final int SPLASH_DISPLAY_LENGTH = 5500;
    public Thread thread = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView_logo);
        textView = findViewById(R.id.text);
        transitionsContainer = findViewById(R.id.linear_layout);

        TransitionManager.beginDelayedTransition(transitionsContainer,
                new ChangeText().setChangeBehavior(ChangeText.CHANGE_BEHAVIOR_OUT_IN));


        imageView.setVisibility(View.VISIBLE);
        textView.setVisibility(View.VISIBLE);

        Animation animation1 = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fade);
        imageView.startAnimation(animation1);
        TransitionManager.beginDelayedTransition(transitionsContainer,
                new ChangeText().setChangeBehavior(ChangeText.CHANGE_BEHAVIOR_OUT_IN));

        thread = new Thread() {

            @Override
            public void run () {
                try {
                    while (!thread.isInterrupted()) {
                        Thread.sleep(2000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run () {
                                if (mSecondText == true) {
                                    mSecondText = false;
                                } else {
                                    mSecondText = true;
                                }
                                textView.setText(mSecondText ? TEXT_1 : TEXT_2);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        thread.start();

        continueApplication();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run () {
                Intent mainIntent = new Intent(MainActivity.this, Language.class);
                MainActivity.this.startActivity(mainIntent);
                MainActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

    }

    private void continueApplication () {
    }
}
