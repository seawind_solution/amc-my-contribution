package com.seawind.amc;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class News extends AppCompatActivity {

    private ProgressDialog progressDialog;
    public String get_id, is_child;
    RecyclerView recyclerView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    News_Adapter adapter;
    String lang;

    ArrayList<Grid_data> arrayList = new ArrayList<>();
    ArrayList<New> arrayList_news = new ArrayList<>();

    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 3;
    int firstVisibleItem, visibleItemCount, totalItemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        progressDialog = ProgressDialog.show(News.this, null, null, false, true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.layout_loading_dialog);
        progressDialog.setCancelable(false);
        progressDialog.dismiss();
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        recyclerView = findViewById(R.id.id_Recycle_view);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(News.this);
        recyclerView.setLayoutManager(llm);
        adapter = new News_Adapter(News.this, arrayList_news);
        recyclerView.setAdapter(adapter);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshItems();
            }
        });

        Call_one();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = llm.getItemCount();
                firstVisibleItem = llm.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    // End has been reached

                    Log.i("Yaeye!", "end called");

                    // Do something

                    loading = true;
                }
            }
        });

    }

    void refreshItems() {
        // Load items
        Call_one();
        // Load complete
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...

        // Stop refresh animation
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void Call_one() {

        /*if ((lang = AppConstant.getPreferences(News.this, "lang")).equals("English")) {
            AppConstant.URL_BASE = Url.URL_BASE_ENGLISH;
        } else if ((lang = AppConstant.getPreferences(News.this, "lang")).equals("Hindi")) {
            AppConstant.URL_BASE = Url.URL_BASE_HINDI;
        } else if ((lang = AppConstant.getPreferences(News.this, "lang")).equals("Gujrati")) {
            AppConstant.URL_BASE = Url.URL_BASE_GUJRATI;
        }*/
        try {
            progressDialog.show();
            Url.getWebService().getLatestNewsData().enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    arrayList_news.clear();
                    if (response.isSuccessful()) {
                        JSONObject responseObject = AppConstant.getResponseObject(response);
                        if (responseObject != null) {
                            if (responseObject.optBoolean("IsSuccess")) {
                                List<New> temp_survey = new Gson().fromJson(responseObject
                                        .optString("ResponseData"), new TypeToken<List<New>>() {
                                }.getType());
                                arrayList_news.addAll(temp_survey);

                                adapter.notifyDataSetChanged();
                            } else {
                                Toast.makeText(News.this, responseObject.optString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(News.this, "r", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(News.this, "r", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(News.this, "r", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            try {
                progressDialog.dismiss();
            }catch (Exception e1){

            }
            Toast.makeText(News.this,  R.string.error_amc, Toast.LENGTH_SHORT).show();
        }

    }
}
