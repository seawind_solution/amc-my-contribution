package com.seawind.amc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ronak Gopani on 31/1/19 at 6:41 PM.
 */
public class News_p {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Slug")
    @Expose
    private String slug;
    @SerializedName("Content")
    @Expose
    private String content;
    @SerializedName("ThumbImg")
    @Expose
    private String thumbImg;
    @SerializedName("YoutubeUrl")
    @Expose
    private String youtubeUrl;
    @SerializedName("NewsViews")
    @Expose
    private String newsViews;
    @SerializedName("NewsShared")
    @Expose
    private String newsShared;
    @SerializedName("EntDt")
    @Expose
    private String entDt;
    @SerializedName("EntBy")
    @Expose
    private String entBy;
    @SerializedName("ModDt")
    @Expose
    private String modDt;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("NewsImg")
    @Expose
    private List<String> newsImg = new ArrayList<>();
    @SerializedName("NewsVideo")
    @Expose
    private List<String> newsVideo = new ArrayList<>();
    @SerializedName("NewsAudio")
    @Expose
    private List<String> newsAudio = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getThumbImg() {
        return thumbImg;
    }

    public void setThumbImg(String thumbImg) {
        this.thumbImg = thumbImg;
    }

    public String getYoutubeUrl() {
        return youtubeUrl;
    }

    public void setYoutubeUrl(String youtubeUrl) {
        this.youtubeUrl = youtubeUrl;
    }

    public String getNewsViews() {
        return newsViews;
    }

    public void setNewsViews(String newsViews) {
        this.newsViews = newsViews;
    }

    public String getNewsShared() {
        return newsShared;
    }

    public void setNewsShared(String newsShared) {
        this.newsShared = newsShared;
    }

    public String getEntDt() {
        return entDt;
    }

    public void setEntDt(String entDt) {
        this.entDt = entDt;
    }

    public String getEntBy() {
        return entBy;
    }

    public void setEntBy(String entBy) {
        this.entBy = entBy;
    }

    public String getModDt() {
        return modDt;
    }

    public void setModDt(String modDt) {
        this.modDt = modDt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getNewsImg() {
        return newsImg;
    }

    public void setNewsImg(List<String> newsImg) {
        this.newsImg = newsImg;
    }

    public List<String> getNewsVideo() {
        return newsVideo;
    }

    public void setNewsVideo(List<String> newsVideo) {
        this.newsVideo = newsVideo;
    }

    public List<String> getNewsAudio() {
        return newsAudio;
    }

    public void setNewsAudio(List<String> newsAudio) {
        this.newsAudio = newsAudio;
    }

}
