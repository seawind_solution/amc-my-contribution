package com.seawind.amc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ronak Gopani on 16/10/19 at 5:06 PM.
 */
public class New {

    @SerializedName("Id")
    @Expose
    private String Id;
    @SerializedName("Title")
    @Expose
    private String Title;
    @SerializedName("Content")
    @Expose
    private String Content;
    @SerializedName("ThumbImg")
    @Expose
    private String ThumbImg;
    @SerializedName("EntDt")
    @Expose
    private String EntDt;
    @SerializedName("EntBy")
    @Expose
    private String EntBy;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getThumbImg() {
        return ThumbImg;
    }

    public void setThumbImg(String thumbImg) {
        ThumbImg = thumbImg;
    }

    public String getEntDt() {
        return EntDt;
    }

    public void setEntDt(String entDt) {
        EntDt = entDt;
    }

    public String getEntBy() {
        return EntBy;
    }

    public void setEntBy(String entBy) {
        EntBy = entBy;
    }
}
