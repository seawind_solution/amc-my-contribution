package com.seawind.amc;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Locale;

public class Language extends AppCompatActivity {

    private String lang;
    private Resources res;
    private DisplayMetrics dm;
    private Configuration conf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        CardView eng = findViewById(R.id.eng);
        CardView guj = findViewById(R.id.guj);
        FloatingActionButton fab = findViewById(R.id.fabNext);

        eng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.savePreferences(Language.this, "lang", "Hindi");
                AppConstant.URL_BASE = Url.URL_BASE_HINDI;

                lang = AppConstant.getPreferences(Language.this, "lang");
                res = Language.this.getResources();
                dm = res.getDisplayMetrics();
                conf = res.getConfiguration();
                conf.setLocale(new Locale("en"));
                res.updateConfiguration(conf, dm);

                AppConstant.savePreferences(Language.this, "lang", lang);
                Intent intent = new Intent(Language.this,Selection.class);
                startActivity(intent);
            }
        });

        guj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppConstant.savePreferences(Language.this, "lang", "Hindi");
                AppConstant.URL_BASE = Url.URL_BASE_HINDI;

                lang = AppConstant.getPreferences(Language.this, "lang");
                res = Language.this.getResources();
                dm = res.getDisplayMetrics();
                conf = res.getConfiguration();
                conf.setLocale(new Locale("gu"));
                res.updateConfiguration(conf, dm);

                AppConstant.savePreferences(Language.this, "lang", lang);
                Intent intent = new Intent(Language.this,Selection.class);
                startActivity(intent);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
