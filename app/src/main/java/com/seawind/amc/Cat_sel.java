package com.seawind.amc;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class Cat_sel extends AppCompatActivity {

    public static String base_cat;
    public static Cat_sel cat_selection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cat_sel);

        CardView crd_dan = findViewById(R.id.crd_dan);
        CardView crd_vol = findViewById(R.id.crd_vol);
        CardView crd_food = findViewById(R.id.crd_food);
        CardView crd_helth = findViewById(R.id.crd_helth);

        cat_selection = this;

        if (Selection.base.equals("vol")) {
            crd_dan.setVisibility(View.VISIBLE);
            crd_vol.setVisibility(View.VISIBLE);
        } else {
            crd_dan.setVisibility(View.GONE);
            crd_vol.setVisibility(View.GONE);
        }


        crd_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                base_cat = "food";
                String temp = Selection.base;

                if (temp.equals("need")) {
                    Intent intent = new Intent(Cat_sel.this, Need.class);
                    startActivity(intent);
                } else if (temp.equals("vol")) {
                    Intent intent = new Intent(Cat_sel.this, Vol.class);
                    startActivity(intent);
                }

            }
        });

        crd_helth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                base_cat = "health";
                String temp = Selection.base;

                if (temp.equals("need")) {
                    Intent intent = new Intent(Cat_sel.this, Need_H.class);
                    startActivity(intent);
                } else if (temp.equals("vol")) {
                    Intent intent = new Intent(Cat_sel.this, Vol_H.class);
                    startActivity(intent);
                }
            }
        });

        crd_dan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Cat_sel.this, Shramdan_one.class);
                startActivity(intent);
            }
        });

        crd_vol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Cat_sel.this, Sel_Vlo_one.class);
                startActivity(intent);
            }
        });
    }
}
