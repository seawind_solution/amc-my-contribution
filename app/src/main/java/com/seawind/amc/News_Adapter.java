package com.seawind.amc;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

class News_Adapter extends RecyclerView.Adapter<News_Adapter.ViewHolder> {

    public String CustomerId, Type, Title, Lat, Lng, address;
    public Context context;
    private List<New> arrayList;

    private OnRecyclerViewClickListener mOnClickListener = null;

    public News_Adapter(Context context, ArrayList<New> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public interface OnRecyclerViewClickListener {
        void onItemClick(View view, int id);
    }

    public void setOnItemClickListener(OnRecyclerViewClickListener listener) {
        this.mOnClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.thumb_news_ui, viewGroup, false);

        ViewHolder V = new ViewHolder(view);
        return V;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        New booking_address = arrayList.get(i);

        viewHolder.tv_thumb_title.setText(booking_address.getTitle());
        viewHolder.tv_thumb_by.setText(booking_address.getEntBy());
        viewHolder.tv_thumb_date.setText(booking_address.getEntDt());

        Glide.with(context).load(arrayList.get(i).getThumbImg())
                .apply(RequestOptions.placeholderOf(R.drawable.logo)).into(viewHolder.ig_thumb_image);

        viewHolder.card_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(v.getContext(), Detail_news.class);
                    intent.putExtra("Id", booking_address.getId());
                    v.getContext().startActivity(intent);
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_thumb_title, tv_thumb_by, tv_thumb_date;
        LinearLayout card_date;
        ImageView ig_thumb_image;

        ViewHolder(View view) {
            super(view);
            tv_thumb_title = (TextView) view.findViewById(R.id.tv_thumb_title);
            tv_thumb_by = (TextView) view.findViewById(R.id.tv_thumb_by);
            tv_thumb_date = (TextView) view.findViewById(R.id.tv_thumb_date);
            card_date = view.findViewById(R.id.card_date);
            ig_thumb_image = view.findViewById(R.id.ig_thumb_image);
        }
    }
}
