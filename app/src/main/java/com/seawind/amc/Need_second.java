package com.seawind.amc;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Need_second extends AppCompatActivity {

    Button bt_submit;
    Spinner sp_o, sp_t, sp_th;
    ProgressDialog progressDialog;
    ArrayList<String> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_need_second);

        String[] o = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"};
        String[] t = {"Lunch", "Dinner", "Both"};
        String[] th = {"1 Day", "2 Day", "3 Day", "4 Day", "5 Day", "6 Day", "1 Week", "2 Week", "3 Week", "4 Week", "1 Month"};

        bt_submit = findViewById(R.id.bt_submit);
        sp_o = (Spinner) findViewById(R.id.sp_person);
        sp_t = (Spinner) findViewById(R.id.sp_day);
        sp_th = (Spinner) findViewById(R.id.sp_period);

        progressDialog = ProgressDialog.show(Need_second.this, null, null, false, true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.layout_loading_dialog);
        progressDialog.setCancelable(false);
        progressDialog.dismiss();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Need_second.this, android.R.layout.simple_spinner_dropdown_item, o);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_o.setAdapter(adapter);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(Need_second.this, android.R.layout.simple_spinner_dropdown_item, t);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_t.setAdapter(adapter2);

        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(Need_second.this, android.R.layout.simple_spinner_dropdown_item, th);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_th.setAdapter(adapter3);

        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String spring_s1 = (sp_o.getSelectedItem().toString());
                String spring_s2 = (sp_t.getSelectedItem().toString());
                String spring_s3 = (sp_th.getSelectedItem().toString());

                    try {
                        RequestBody CategoryOfHelp = RequestBody.create(MediaType.parse("text/plain"), "Food");
                        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), Need.name_s);
                        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), Need.phone_s);
                        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), Need.email_s);
                        RequestBody pin = RequestBody.create(MediaType.parse("text/plain"), Need.pin_s);
                        RequestBody add = RequestBody.create(MediaType.parse("text/plain"), Need.add_s);
                        RequestBody woard = RequestBody.create(MediaType.parse("text/plain"), Need.spring_s);

                        RequestBody spring__s1 = RequestBody.create(MediaType.parse("text/plain"), spring_s1);
                        RequestBody spring__s2 = RequestBody.create(MediaType.parse("text/plain"), spring_s2);
                        RequestBody spring__s3 = RequestBody.create(MediaType.parse("text/plain"), spring_s3);
                        RequestBody helth_issue = RequestBody.create(MediaType.parse("text/plain"), "N/A");

                        progressDialog.show();

                        Url.getWebService().sevaLevaRegistration(CategoryOfHelp, name, phone, email, woard, pin, add, spring__s1, spring__s2, spring__s3, helth_issue).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                progressDialog.dismiss();
                                if (response.isSuccessful()) {
                                    try {
                                        JSONObject responseObject = AppConstant.getResponseObject(response);

                                        if (responseObject.optBoolean("IsSuccess")) {
                                            String detail = responseObject.optString("ResponseData");
                                            try {

                                                AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(Need_second.this);
                                                myAlertDialog.setMessage("Your request has been send successfully");
                                                myAlertDialog.setPositiveButton("ok",
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface arg0, int arg1) {
                                                                Intent intent = new Intent(Need_second.this, Selection.class);
                                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                startActivity(intent);
                                                            }
                                                        });
                                                myAlertDialog.setCancelable(false);
                                                myAlertDialog.show();

                                            } catch (Exception e) {

                                            }
                                        } else {
                                            Toast.makeText(Need_second.this, responseObject.optString("Message"), Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    Toast.makeText(Need_second.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Toast.makeText(Need_second.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                                Log.d("", "Error in Ticket Category : " + t.getMessage());
                            }
                        });
                    } catch (Exception e) {

                        try {
                            progressDialog.dismiss();
                        } catch (Exception e1) {

                        }

                        Toast.makeText(Need_second.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                    }

            }
        });
    }
}
