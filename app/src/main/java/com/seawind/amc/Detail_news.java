package com.seawind.amc;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.text.HtmlCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Detail_news extends AppCompatActivity {

    int Id;
    ImageView newsImages;
    TextView title, tv_by, tv_date, description;
    ImageView videos, images, audios;
    News_p _news_p;
    ViewPager viewPager;
    String lang;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_news);
        viewPager = (ViewPager) findViewById(R.id.pagerImgNewsDetail);
        title = findViewById(R.id.title);
        description = findViewById(R.id.description);
        tv_by = findViewById(R.id.tv_by);
        tv_date = findViewById(R.id.tv_date);

        images = findViewById(R.id.images);
        videos = findViewById(R.id.videos);
        audios = findViewById(R.id.audios);

        progressDialog = ProgressDialog.show(Detail_news.this, null, null, false, true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.layout_loading_dialog);
        progressDialog.setCancelable(false);
        progressDialog.show();

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            Id = Integer.parseInt(String.valueOf(0));
        } else {
            Id = Integer.parseInt(extras.getString("Id"));
        }

        /*if ((lang = AppConstant.getPreferences(Detail_news.this, "lang")).equals("English")) {
            AppConstant.URL_BASE = Url.URL_BASE_ENGLISH;
            System.out.println(lang);
        } else if ((lang = AppConstant.getPreferences(Detail_news.this, "lang")).equals("Hindi")) {
            AppConstant.URL_BASE = Url.URL_BASE_HINDI;
            System.out.println(lang);
        } else if ((lang = AppConstant.getPreferences(Detail_news.this, "lang")).equals("Gujrati")) {
            AppConstant.URL_BASE = Url.URL_BASE_HINDI;
            System.out.println(lang);
        }*/

        try {
            progressDialog.show();
            Url.getWebService().getNewsDetailById(String.valueOf(Id)).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    progressDialog.dismiss();
                    if (response.isSuccessful()) {
                        try {
                            JSONObject responseObject = new JSONObject(response.body().string());
                            if (responseObject.optBoolean("IsSuccess")) {
                                _news_p = new Gson().fromJson(responseObject.optString("ResponseData"),
                                        new TypeToken<News_p>() {
                                        }.getType());
                                setNewsDetail();
                            } else {
                                Toast.makeText(Detail_news.this, responseObject.optString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(Detail_news.this, "l", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            try {
                progressDialog.dismiss();
            } catch (Exception e1) {

            }
            Toast.makeText(this, R.string.error_amc, Toast.LENGTH_SHORT).show();
        }

        videos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> videolist = new ArrayList<>();
                videolist.clear();
                if (_news_p.getNewsVideo().size() != 0) {
                    videolist.addAll(_news_p.getNewsVideo());
                }

                VideoAdapter adapterView = new VideoAdapter(Detail_news.this, videolist);

                viewPager.setAdapter(adapterView);
            }
        });

        images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> imageList = new ArrayList<>();
                imageList.clear();
                imageList.add(_news_p.getThumbImg());
                if (_news_p.getNewsImg().size() != 0) {
                    imageList.addAll(_news_p.getNewsImg());
                }
                ImageAdapter adapterView = new ImageAdapter(Detail_news.this, imageList);

                viewPager.setAdapter(adapterView);
            }
        });

        audios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> audioList = new ArrayList<>();
                audioList.clear();
                if (_news_p.getNewsVideo().size() != 0) {
                    audioList.addAll(_news_p.getNewsVideo());
                }

                AudioAdapter adapterView = new AudioAdapter(Detail_news.this, audioList);

                viewPager.setAdapter(adapterView);
            }
        });
    }

    @SuppressLint({"SetTextI18n", "SetJavaScriptEnabled"})
    private void setNewsDetail() {

        try {
            List<String> imageList = new ArrayList<>();
            imageList.clear();
            imageList.add(_news_p.getThumbImg());
            if (_news_p.getNewsImg().size() != 0) {
                imageList.addAll(_news_p.getNewsImg());
            }
            ImageAdapter adapterView = new ImageAdapter(Detail_news.this, imageList);

            viewPager.setAdapter(adapterView);
            title.setText(_news_p.getTitle());

     /*   if (_news_p.getNewsVideo().size() != 0) {
            getLayoutVideo().setVisibility(View.VISIBLE);
            getPagerVideo().setAdapter(new NewsDetailVideoPagerAdapter(SingleNewsActivity.this, _news_p.getNewsVideo()));
            getIndicatorVideo().setViewPager(getPagerVideo());
        } else {
            getLayoutVideo().setVisibility(View.GONE);
        }
        if (_news_p.getNewsAudio().size() != 0) {
            getLayoutAudio().setVisibility(View.VISIBLE);
            getPagerAudio().setAdapter(new NewsDetailAudioPagerAdapter(SingleNewsActivity.this, _news_p.getNewsAudio()));
            getIndicatorAudio().setViewPager(getPagerAudio());
        } else {
            getLayoutAudio().setVisibility(View.GONE);
        }
        if (!_news_p.getYoutubeUrl().equals("")) {
            getLayoutYouTube().setVisibility(View.VISIBLE);
            if (_news_p.getYoutubeUrl().contains("embed")) {
                Glide.with(SingleNewsActivity.this).load(AppConstant.getImageURLfromVideoURL(_news_p.getYoutubeUrl()))
                        .apply(RequestOptions.placeholderOf(R.drawable.placeholder_medium)).into(getImgYouTube());
                getImgYouTube().setOnClickListener(v -> {
                    String[] urls = _news_p.getYoutubeUrl().split("embed/");
                    startActivity(new Intent(SingleNewsActivity.this, YouTubePlayerActivity.class)
                            .putExtra("id", urls[1]));
                });
            } else {
                getLayoutYouTube().setVisibility(View.GONE);
            }
        } else {
            getLayoutYouTube().setVisibility(View.GONE);
        }*/

            tv_by.setText("BY " + _news_p.getEntBy());
            tv_date.setText("PUBLISHED: " + (_news_p.getModDt()));
            description.setText(HtmlCompat.fromHtml(_news_p.getContent(), 0));

        /*getFabShare().setOnClickListener(v -> {
            Intent s = new Intent(Intent.ACTION_SEND);
            s.setType("text/plain");
            s.putExtra(Intent.EXTRA_TEXT, AppConstant.SLUG_URL +  _news_p.getSlug());
            startActivity(Intent.createChooser(s, "Share via"));
        });*/
        } catch (Exception e) {

        }

    }
}