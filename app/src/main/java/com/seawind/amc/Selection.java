package com.seawind.amc;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class Selection extends AppCompatActivity {

    public static String base;
    public static Selection selection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);

        selection = this;

        CardView need = findViewById(R.id.need);
        CardView vol = findViewById(R.id.vol);
        CardView news = findViewById(R.id.news);
        CardView records = findViewById(R.id.records);

        need.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base="need";
                Intent intent = new Intent(Selection.this,Cat_sel.class);
                startActivity(intent);
            }
        });

        vol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                base="vol";
                Intent intent = new Intent(Selection.this,Cat_sel.class);
                startActivity(intent);
            }
        });

        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Selection.this,News.class);
                startActivity(intent);
            }
        });

        /*records.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Selection.this,Records.class);
                startActivity(intent);
            }
        });*/
    }
}
