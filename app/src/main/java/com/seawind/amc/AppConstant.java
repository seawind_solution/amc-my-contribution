package com.seawind.amc;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Response;

@SuppressLint("SimpleDateFormat")
public class AppConstant {

    public static String HEADER_KEY = "X-API-KEY";
    public static String HEADER_KEY_VALUE = "123456789123456789";
    public static String PREFERENCES = "NEWS_ONLINE";
    public static String URL_BASE = "https://www.newsonlinelive.tv/Webservices/";

    public static JSONObject getResponseObject(Response<ResponseBody> response) {
        try {
            return new JSONObject(response.body().string());
        } catch (JSONException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void savePreferences(Context context, String key, String value) {
        SharedPreferences preferences = context.getSharedPreferences(AppConstant.PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
        editor.commit();
    }

    public static String getPreferences (Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(AppConstant.PREFERENCES, Context.MODE_PRIVATE);
        return preferences.getString(key, "");
    }

    public static void removePreferences (Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(AppConstant.PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.apply();
        editor.commit();
    }
}
