package com.seawind.amc;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

import static com.seawind.amc.AppConstant.URL_BASE;


/**
 * Created by Ronak Gopani on 29/6/19 at 7:15 PM.
 */
public interface Url {

    String URL_BASE_GUJRATI = "https://www.newsonlinelive.tv/Webservices/";
    String URL_BASE_HINDI = "https://www.1024d.com/Webservices/";
    String URL_BASE_ENGLISH = "https://www.newsonlinelive.tv/English/Webservices/";

    String GET_SPI = "getMasterDetails/{parentId}/{mnId}";
    String USER_REGISTER_L = "sevaLevaRegistration";
    String USER_REGISTER_D = "sevaDevaRegistration";
    String USER_REGISTER_S = "sevaDevaShramDanRegistration";
    String USER_REGISTER_R = "sevaDevaVolunteerRegistration";
    String TOP10NEWS = "getLatestNewsData";
    String GET_NEWS_DETAIL = "getNewsDetailById/{id}";
    String S = "country/{sort}";

    static NewsOnline getWebService() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(10, TimeUnit.MINUTES);
        httpClient.readTimeout(10, TimeUnit.MINUTES);
        httpClient.writeTimeout(10, TimeUnit.MINUTES);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(chain -> {
            Request request = chain.request().newBuilder()
                    .addHeader("Connection", "close")
                    .addHeader(AppConstant.HEADER_KEY, AppConstant.HEADER_KEY_VALUE).build();
            return chain.proceed(request);
        }).addInterceptor(interceptor);
        return new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(URL_BASE).client(httpClient.build()).build().create(NewsOnline.class);
    }

    /*static s s() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(10, TimeUnit.MINUTES);
        httpClient.readTimeout(10, TimeUnit.MINUTES);
        httpClient.writeTimeout(10, TimeUnit.MINUTES);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(chain -> {
            Request request = chain.request().newBuilder()
                    .addHeader("Connection", "close").build();
            return chain.proceed(request);
        }).addInterceptor(interceptor);
        return new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://cor;;l;;ona.lmao.ninja/countries?sort=country/").client(httpClient.build()).build().create(s.class);
    }

    interface s {
        @GET(S)
        Call<ResponseBody> country(
                @Path("sort") RequestBody country);
    }
*/
    interface NewsOnline {

        @GET(GET_SPI)
        Call<ResponseBody> getMasterDetails(
                @Path("parentId") int parentId,
                @Path("mnId") int mnId);

        @Multipart
        @POST(USER_REGISTER_L)
        Call<ResponseBody> sevaLevaRegistration(
                @Part("CategoryOfHelp") RequestBody CategoryOfHelp,
                @Part("Name") RequestBody Name,
                @Part("Phone") RequestBody Phone,
                @Part("Email") RequestBody Email,
                @Part("Word") RequestBody Word,
                @Part("Pin") RequestBody Pin,
                @Part("Address") RequestBody Address,
                @Part("NoOfPeople") RequestBody NoOfPeople,
                @Part("HowManyTimeInDay") RequestBody HowManyTimeInDay,
                @Part("HowLongTimePeriod") RequestBody HowLongTimePeriod,
                @Part("HealthIssue") RequestBody HealthIssue
        );

        @Multipart
        @POST(USER_REGISTER_D)
        Call<ResponseBody> sevaDevaRegistration(
                @Part("CategoryOfHelp") RequestBody CategoryOfHelp,
                @Part("Name") RequestBody Name,
                @Part("Phone") RequestBody Phone,
                @Part("Email") RequestBody Email,
                @Part("Word") RequestBody Word,
                @Part("Pin") RequestBody Pin,
                @Part("Address") RequestBody Address,
                @Part("TypeOfFood") RequestBody TypeOfFood,
                @Part("AnyOtherKhadyaVastu") RequestBody AnyOtherKhadyaVastu,
                @Part("KetalPacketkKetlaKillo") RequestBody KetalPacketkKetlaKillo,
                @Part("NoOfPeople") RequestBody NoOfPeople,
                @Part("HowManyTimeInDay") RequestBody HowManyTimeInDay,
                @Part("HowLongTimePeriod") RequestBody HowLongTimePeriod,
                @Part("HealthIssue") RequestBody HealthIssue,
                @Part("DeliveryByAmcOrSelf") RequestBody DeliveryByAmcOrSelf,
                @Part("OrganizationOrIndividual") RequestBody OrganizationOrIndividual
        );

        @Multipart
        @POST(USER_REGISTER_S)
        Call<ResponseBody> sevaDevaShramDanRegistration(
                @Part("Name") RequestBody Name,
                @Part("Phone") RequestBody Phone,
                @Part("Email") RequestBody Email,
                @Part("Word") RequestBody Word,
                @Part("Pin") RequestBody Pin,
                @Part("Address") RequestBody Address,
                @Part("How") RequestBody How,
                @Part("OwnVehicle") RequestBody OwnVehicle,
                @Part("HaveMaskAndSanitisation") RequestBody HaveMaskAndSanitisation,
                @Part("ManageFood") RequestBody ManageFood,
                @Part("Education") RequestBody Education,
                @Part("Area") RequestBody Area
        );

        @Multipart
        @POST(USER_REGISTER_R)
        Call<ResponseBody> sevaDevaVolunteerRegistration(
                @Part("Name") RequestBody Name,
                @Part("Phone") RequestBody Phone,
                @Part("Email") RequestBody Email,
                @Part("Word") RequestBody Word,
                @Part("Pin") RequestBody Pin,
                @Part("Address") RequestBody Address,
                @Part("TypeOfVolunteer") RequestBody TypeOfVolunteer,
                @Part("WichFieldDoctor") RequestBody WichFieldDoctor,
                @Part("QualificationOfDoctor") RequestBody QualificationOfDoctor,
                @Part("WorkType") RequestBody WorkType
        );

        @GET(GET_NEWS_DETAIL)
        Call<ResponseBody> getNewsDetailById(
                @Path("id") String newsId
        );

        @GET(TOP10NEWS)
        Call<ResponseBody> getLatestNewsData();
    }
}
