package com.seawind.amc;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Shramdan_sec extends AppCompatActivity {
    Button bt_submit;
    Spinner sp_o, sp_th;
    ProgressDialog progressDialog;
    RadioGroup rd_radio_payment, rd_radio_payment2, rd_radio_payment3;
    RequestBody mode__S1, mode__S2, mode__S3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shramdan_sec);

        rd_radio_payment = findViewById(R.id.rg_extend_payment1);
        rd_radio_payment2 = findViewById(R.id.rg_extend_payment2);
        rd_radio_payment3 = findViewById(R.id.rg_extend_payment3);

        String[] o = {"For distributing food from own vehicles", "Distributing vegetables to my own society"};
        String[] th = {"BAMS", "BDS", "Graduate, Master's", "Gratuate/PostGraduate", "Health professsional", "IT/Computer Expert"
                , "Nursing staff", "10th pass", "12th pass"};

        bt_submit = findViewById(R.id.bt_submit);
        sp_o = (Spinner) findViewById(R.id.sp_person);
        sp_th = (Spinner) findViewById(R.id.sp_period);

        progressDialog = ProgressDialog.show(Shramdan_sec.this, null, null, false, true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.layout_loading_dialog);
        progressDialog.setCancelable(false);
        progressDialog.dismiss();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Shramdan_sec.this, android.R.layout.simple_spinner_dropdown_item, o);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_o.setAdapter(adapter);

        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(Shramdan_sec.this, android.R.layout.simple_spinner_dropdown_item, th);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_th.setAdapter(adapter3);

        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String spring_s1 = (sp_o.getSelectedItem().toString());
                String spring_s3 = (sp_th.getSelectedItem().toString());

                try {

                    RequestBody name = RequestBody.create(MediaType.parse("text/plain"), Shramdan_one.name_s);
                    RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), Shramdan_one.phone_s);
                    RequestBody email = RequestBody.create(MediaType.parse("text/plain"), Shramdan_one.email_s);
                    RequestBody pin = RequestBody.create(MediaType.parse("text/plain"), Shramdan_one.pin_s);
                    RequestBody add = RequestBody.create(MediaType.parse("text/plain"), Shramdan_one.add_s);
                    RequestBody woard = RequestBody.create(MediaType.parse("text/plain"), Shramdan_one.spring_s);

                    RequestBody spring__s1 = RequestBody.create(MediaType.parse("text/plain"), spring_s1);
                    RequestBody spring__s3 = RequestBody.create(MediaType.parse("text/plain"), spring_s3);
                    RequestBody helth_issue = RequestBody.create(MediaType.parse("text/plain"), "N/A");
                    RequestBody area = RequestBody.create(MediaType.parse("text/plain"), "N/A");

                    int selectedId = rd_radio_payment.getCheckedRadioButtonId();
                    RadioButton radioSexButton = (RadioButton) findViewById(selectedId);
                    String mode = String.valueOf(radioSexButton.getText());
                    mode__S1 = RequestBody.create(MediaType.parse("text/plain"), mode);

                    int selectedId2 = rd_radio_payment2.getCheckedRadioButtonId();
                    RadioButton radioSexButton2 = (RadioButton) findViewById(selectedId2);
                    String mode2 = String.valueOf(radioSexButton2.getText());
                    mode__S2 = RequestBody.create(MediaType.parse("text/plain"), mode2);

                    int selectedId3 = rd_radio_payment.getCheckedRadioButtonId();
                    RadioButton radioSexButton3 = (RadioButton) findViewById(selectedId3);
                    String mode3 = String.valueOf(radioSexButton3.getText());
                    mode__S3 = RequestBody.create(MediaType.parse("text/plain"), mode3);

                    progressDialog.show();

                    Url.getWebService().sevaDevaShramDanRegistration(name, phone, email, woard, pin, add, spring__s1, mode__S1, mode__S2, mode__S3, spring__s3, area).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            progressDialog.dismiss();
                            if (response.isSuccessful()) {
                                try {
                                    JSONObject responseObject = AppConstant.getResponseObject(response);

                                    String detail = responseObject.optString("ResponseData");
                                    System.out.println(detail);
                                    if (responseObject.optBoolean("IsSuccess")) {

                                        try {

                                            AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(Shramdan_sec.this);
                                            myAlertDialog.setMessage("Your request has been send successfully");
                                            myAlertDialog.setPositiveButton("ok",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface arg0, int arg1) {
                                                            Intent intent = new Intent(Shramdan_sec.this, Selection.class);
                                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                            startActivity(intent);
                                                        }
                                                    });
                                            myAlertDialog.setCancelable(false);
                                            myAlertDialog.show();

                                        } catch (Exception e) {

                                        }
                                    } else {
                                        Toast.makeText(Shramdan_sec.this, responseObject.optString("Message"), Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else {
                                Toast.makeText(Shramdan_sec.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(Shramdan_sec.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                            Log.d("", "Error in Ticket Category : " + t.getMessage());
                        }
                    });
                } catch (Exception e) {

                    try {
                        progressDialog.dismiss();
                    } catch (Exception e1) {

                    }

                    Toast.makeText(Shramdan_sec.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
