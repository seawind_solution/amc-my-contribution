package com.seawind.amc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import okhttp3.RequestBody;

public class Vol_Sec extends AppCompatActivity {

    EditText HealthIssue;
    FloatingActionButton bt_submit;
    Spinner sp_o, sp_t, sp_th, sp_food_type, sp_much;
    ProgressDialog progressDialog;
    public static Vol_Sec vol_sec;
    RequestBody mode__S1, mode__S2;
    public static String helthissue, spring_s1, spring_s2, spring_s3, spring_s4, spring_s5;
    ArrayList<String> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vol_sec);

        vol_sec = this;

        String[] o = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"};
        String[] t = {"Lunch", "Dinner", "Both"};
        String[] much = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"};
        String[] cook = {"Food Packet", "Cooked Food"};
        String[] th = {"1 Day", "2 Day", "3 Day", "4 Day", "5 Day", "6 Day", "1 Week", "2 Week", "3 Week", "4 Week", "1 Month"};

        HealthIssue = findViewById(R.id.ed_other);
        bt_submit = findViewById(R.id.fabNext);
        sp_food_type = (Spinner) findViewById(R.id.sp_food_type);
        sp_much = (Spinner) findViewById(R.id.sp_much);
        sp_o = (Spinner) findViewById(R.id.sp_person);
        sp_t = (Spinner) findViewById(R.id.sp_day);
        sp_th = (Spinner) findViewById(R.id.sp_period);

        progressDialog = ProgressDialog.show(Vol_Sec.this, null, null, false, true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.layout_loading_dialog);
        progressDialog.setCancelable(false);
        progressDialog.dismiss();

        ArrayAdapter<String> adapter4 = new ArrayAdapter<String>(Vol_Sec.this, android.R.layout.simple_spinner_dropdown_item, cook);
        adapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_food_type.setAdapter(adapter4);

        ArrayAdapter<String> adapter5 = new ArrayAdapter<String>(Vol_Sec.this, android.R.layout.simple_spinner_dropdown_item, much);
        adapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_much.setAdapter(adapter5);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Vol_Sec.this, android.R.layout.simple_spinner_dropdown_item, o);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_o.setAdapter(adapter);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(Vol_Sec.this, android.R.layout.simple_spinner_dropdown_item, t);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_t.setAdapter(adapter2);

        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(Vol_Sec.this, android.R.layout.simple_spinner_dropdown_item, th);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_th.setAdapter(adapter3);

        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                helthissue = HealthIssue.getText().toString();
                spring_s1 = (sp_o.getSelectedItem().toString());
                spring_s2 = (sp_t.getSelectedItem().toString());
                spring_s3 = (sp_th.getSelectedItem().toString());
                spring_s4 = (sp_food_type.getSelectedItem().toString());
                spring_s5 = (sp_much.getSelectedItem().toString());

                if (!helthissue.isEmpty()) {
                    try {

                        Intent intent = new Intent(Vol_Sec.this, Vol_thr.class);
                        startActivity(intent);

                    } catch (Exception e) {

                        Toast.makeText(Vol_Sec.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    HealthIssue.setError("Invalid");
                }
            }
        });
    }
}