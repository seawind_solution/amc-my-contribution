package com.seawind.amc;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Vol_thr extends AppCompatActivity {

    EditText HealthIssue;
    Button bt_submit;
    Spinner sp_o, sp_t, sp_th, sp_food_type, sp_much;
    ProgressDialog progressDialog;
    RadioGroup rd_radio_payment,rd_radio_payment2;
    RequestBody mode__S1, mode__S2;

    ArrayList<String> arrayList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vol_thr);

        rd_radio_payment = findViewById(R.id.rg_extend_payment);
        rd_radio_payment2 = findViewById(R.id.rg_extend_payment2);
        Button submit =findViewById(R.id.bt_submit);

        progressDialog = ProgressDialog.show(Vol_thr.this, null, null, false, true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.layout_loading_dialog);
        progressDialog.setCancelable(false);
        progressDialog.dismiss();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    RequestBody CategoryOfHelp = RequestBody.create(MediaType.parse("text/plain"), "Food");
                    RequestBody name = RequestBody.create(MediaType.parse("text/plain"), Vol.name_s);
                    RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), Vol.phone_s);
                    RequestBody email = RequestBody.create(MediaType.parse("text/plain"), Vol.email_s);
                    RequestBody pin = RequestBody.create(MediaType.parse("text/plain"), Vol.pin_s);
                    RequestBody add = RequestBody.create(MediaType.parse("text/plain"), Vol.add_s);
                    RequestBody woard = RequestBody.create(MediaType.parse("text/plain"), Vol.spring_s);

                    RequestBody spring__s1 = RequestBody.create(MediaType.parse("text/plain"), Vol_Sec.spring_s1);
                    RequestBody spring__s2 = RequestBody.create(MediaType.parse("text/plain"), Vol_Sec.spring_s2);
                    RequestBody spring__s3 = RequestBody.create(MediaType.parse("text/plain"), Vol_Sec.spring_s3);
                    RequestBody spring__s4 = RequestBody.create(MediaType.parse("text/plain"), Vol_Sec.spring_s4);
                    RequestBody spring__s5 = RequestBody.create(MediaType.parse("text/plain"), Vol_Sec.spring_s5);
                    RequestBody helth_issue = RequestBody.create(MediaType.parse("text/plain"), Vol_Sec.helthissue);
                    int selectedId = rd_radio_payment.getCheckedRadioButtonId();
                    RadioButton radioSexButton = (RadioButton) findViewById(selectedId);
                    String mode = String.valueOf(radioSexButton.getText());
                    mode__S1 = RequestBody.create(MediaType.parse("text/plain"), mode);

                    int selectedId2 = rd_radio_payment2.getCheckedRadioButtonId();
                    RadioButton radioSexButton2 = (RadioButton) findViewById(selectedId2);
                    String mode2 = String.valueOf(radioSexButton2.getText());
                    mode__S2 = RequestBody.create(MediaType.parse("text/plain"), mode2);

                    progressDialog.show();

                    Url.getWebService().sevaDevaRegistration(CategoryOfHelp, name, phone, email, woard, pin, add, spring__s4, helth_issue, spring__s5, spring__s1, spring__s2, spring__s3, helth_issue, mode__S1, mode__S2).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            progressDialog.dismiss();
                            if (response.isSuccessful()) {
                                try {
                                    JSONObject responseObject = AppConstant.getResponseObject(response);

                                    if (responseObject.optBoolean("IsSuccess")) {
                                        String detail = responseObject.optString("ResponseData");
                                        System.out.println(detail);
                                        try {

                                            AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(Vol_thr.this);
                                            myAlertDialog.setMessage("Your request has been send successfully");
                                            myAlertDialog.setPositiveButton("ok",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface arg0, int arg1) {
                                                            Intent intent = new Intent(Vol_thr.this, Selection.class);
                                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                            startActivity(intent);
                                                        }
                                                    });
                                            myAlertDialog.setCancelable(false);
                                            myAlertDialog.show();

                                        } catch (Exception e) {

                                        }
                                    } else {
                                        Toast.makeText(Vol_thr.this, responseObject.optString("Message"), Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else {
                                Toast.makeText(Vol_thr.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(Vol_thr.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                            Log.d("", "Error in Ticket Category : " + t.getMessage());
                        }
                    });
                } catch (Exception e) {

                    try {
                        progressDialog.dismiss();
                    } catch (Exception e1) {

                    }

                    Toast.makeText(Vol_thr.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
