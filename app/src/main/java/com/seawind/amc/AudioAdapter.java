package com.seawind.amc;

/**
 * Created by Ronak Gopani on 17/12/19 at 11:13 AM.
 */

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.List;

public class AudioAdapter extends PagerAdapter {
    Context mContext;
    private List<String> newsImageList;

    AudioAdapter(Context context, List<String> newsImageList) {
        this.mContext = context;
        this.newsImageList = newsImageList;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((VideoView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        VideoView videoView = new VideoView(mContext);

        videoView.setVideoPath(newsImageList.get(position));
        ((ViewPager) container).addView(videoView, 0);

        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, PlayerActivity.class)
                        .putExtra("videoUrl", newsImageList.get(position))
                        .putExtra("isLive", false)
                        .putExtra("type", "audio"));
            }
        });

        return videoView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((VideoView) object);
    }

    @Override
    public int getCount() {
        return newsImageList.size();
    }
}
