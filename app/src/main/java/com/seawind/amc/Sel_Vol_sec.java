package com.seawind.amc;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Sel_Vol_sec extends AppCompatActivity {

    Button bt_submit;
    Spinner sp_vol, sp_field, sp_work_from_home, sp_period;
    ProgressDialog progressDialog;
    TextView t1, t2,t;
    CardView crd1, crd2,c;
    RequestBody spring__s1, spring__s2, spring__s3, spring__s4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sel_vol_sec);

        sp_vol = findViewById(R.id.sp_vol);
        sp_field = findViewById(R.id.sp_field);
        sp_work_from_home = findViewById(R.id.sp_work_from_home);
        sp_period = findViewById(R.id.sp_period);
        t = findViewById(R.id.t);
        t1 = findViewById(R.id.text1);
        t2 = findViewById(R.id.text2);
        c = findViewById(R.id.c);
        crd1 = findViewById(R.id.crd1);
        crd2 = findViewById(R.id.crd2);

        String[] o = {"Eye", "Head","Dentist"};
        String[] type = {"BAMS", "BDS", "Graduate, Master's", "Gratuate/PostGraduate", "Health professsional", "IT/Computer Expert", "Doctor"
                , "Nursing staff", "10th pass", "12th pass"};
        String[] type_3 = {"MD", "MBBS", "BAMS", "ORTHER", "BDS", "OTHER"};
        String[] types_4 = {"Work from home as tele consultant", "work from field as AMC staff doctor"};

        bt_submit = findViewById(R.id.bt_submit);

        progressDialog = ProgressDialog.show(Sel_Vol_sec.this, null, null, false, true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.layout_loading_dialog);
        progressDialog.setCancelable(false);
        progressDialog.dismiss();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Sel_Vol_sec.this, android.R.layout.simple_spinner_dropdown_item, type);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_vol.setAdapter(adapter);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(Sel_Vol_sec.this, android.R.layout.simple_spinner_dropdown_item, o);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_field.setAdapter(adapter2);

        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(Sel_Vol_sec.this, android.R.layout.simple_spinner_dropdown_item, type_3);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_work_from_home.setAdapter(adapter3);

        ArrayAdapter<String> adapter4 = new ArrayAdapter<String>(Sel_Vol_sec.this, android.R.layout.simple_spinner_dropdown_item, types_4);
        adapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_period.setAdapter(adapter4);


        sp_vol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                System.out.println(parentView.getItemAtPosition(position));
                String a = String.valueOf(position);
                if (a.equals("6")) {
                    crd1.setVisibility(View.VISIBLE);
                    c.setVisibility(View.VISIBLE);
                    t.setVisibility(View.VISIBLE);
                    crd2.setVisibility(View.VISIBLE);
                    t1.setVisibility(View.VISIBLE);
                    t2.setVisibility(View.VISIBLE);
                } else {
                    c.setVisibility(View.GONE);
                    t.setVisibility(View.GONE);
                    crd1.setVisibility(View.GONE);
                    crd2.setVisibility(View.GONE);
                    t1.setVisibility(View.GONE);
                    t2.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String spring_s1 = (sp_vol.getSelectedItem().toString());
                String spring_s2 = (sp_field.getSelectedItem().toString());
                String spring_s4 = (sp_period.getSelectedItem().toString());
                String spring_s3 = (sp_work_from_home.getSelectedItem().toString());

                try {
                    RequestBody name = RequestBody.create(MediaType.parse("text/plain"), Sel_Vlo_one.name_s);
                    RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), Sel_Vlo_one.phone_s);
                    RequestBody email = RequestBody.create(MediaType.parse("text/plain"), Sel_Vlo_one.email_s);
                    RequestBody pin = RequestBody.create(MediaType.parse("text/plain"), Sel_Vlo_one.pin_s);
                    RequestBody add = RequestBody.create(MediaType.parse("text/plain"), Sel_Vlo_one.add_s);
                    RequestBody woard = RequestBody.create(MediaType.parse("text/plain"), Sel_Vlo_one.spring_s);
                    spring__s2 = RequestBody.create(MediaType.parse("text/plain"), spring_s2);
                    spring__s1 = RequestBody.create(MediaType.parse("text/plain"), spring_s1);
                    spring__s3 = RequestBody.create(MediaType.parse("text/plain"), spring_s3);
                    spring__s4 = RequestBody.create(MediaType.parse("text/plain"), spring_s4);

                    progressDialog.show();

                    Url.getWebService().sevaDevaVolunteerRegistration(name, phone, email, woard, pin, add, spring__s1, spring__s2, spring__s3, spring__s4).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            progressDialog.dismiss();
                            if (response.isSuccessful()) {
                                try {
                                    JSONObject responseObject = AppConstant.getResponseObject(response);

                                    String detail = responseObject.optString("ResponseData");
                                    System.out.println(detail);
                                    if (responseObject.optBoolean("IsSuccess")) {

                                        try {
                                            AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(Sel_Vol_sec.this);
                                            myAlertDialog.setMessage("Your request has been send successfully");
                                            myAlertDialog.setPositiveButton("ok",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface arg0, int arg1) {
                                                            Intent intent = new Intent(Sel_Vol_sec.this, Selection.class);
                                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                            startActivity(intent);
                                                        }
                                                    });
                                            myAlertDialog.setCancelable(false);
                                            myAlertDialog.show();

                                        } catch (Exception e) {

                                        }
                                    } else {
                                        Toast.makeText(Sel_Vol_sec.this, responseObject.optString("Message"), Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else {
                                Toast.makeText(Sel_Vol_sec.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(Sel_Vol_sec.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                            Log.d("", "Error in Ticket Category : " + t.getMessage());
                        }
                    });
                } catch (Exception e) {

                    try {
                        progressDialog.dismiss();
                    } catch (Exception e1) {

                    }

                    Toast.makeText(Sel_Vol_sec.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
