package com.seawind.amc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

class Record_Adapter extends RecyclerView.Adapter<Record_Adapter.ViewHolder> {

    public String CustomerId, Type, Title, Lat, Lng, address;
    public Context context;
    private List<Rec> arrayList;

    private OnRecyclerViewClickListener mOnClickListener = null;

    public Record_Adapter(Context context, ArrayList<Rec> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public interface OnRecyclerViewClickListener {
        void onItemClick(View view, int id);
    }

    public void setOnItemClickListener(OnRecyclerViewClickListener listener) {
        this.mOnClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.record_ui, viewGroup, false);

        ViewHolder V = new ViewHolder(view);
        return V;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Rec rec = arrayList.get(i);

        viewHolder.con.setText(rec.getCountry());
        viewHolder.cas.setText(rec.getCases());
        viewHolder.act.setText(rec.getActive());
        viewHolder.rec.setText(rec.getRecovered());
        viewHolder.ded.setText(rec.getDeaths());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView con,cas,act, rec, ded;


        ViewHolder(View view) {
            super(view);
            con = (TextView) view.findViewById(R.id.con);
            cas = (TextView) view.findViewById(R.id.cas);
            act = (TextView) view.findViewById(R.id.act);
            rec = (TextView) view.findViewById(R.id.rec);
            ded = (TextView) view.findViewById(R.id.ded);
        }
    }
}
