package com.seawind.amc;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Vol_h_s extends AppCompatActivity {

    EditText HealthIssue;
    Button bt_submit;
    //Spinner sp_o, sp_t, sp_th;
    ProgressDialog progressDialog;
    ArrayList<String> arrayList = new ArrayList<>();

    RadioGroup rd_radio_payment,rd_radio_payment2;
    RequestBody mode__S1, mode__S2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vol_h_s);
        String[] o = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"};
        String[] t = {"Lunch", "Dinner", "Both"};
        String[] th = {"1 Day", "2 Day", "3 Day", "4 Day", "5 Day", "6 Day", "1 Week", "2 Week", "3 Week", "4 Week", "1 Month"};

        HealthIssue = findViewById(R.id.ed_other);
        bt_submit = findViewById(R.id.bt_submit);
        rd_radio_payment = findViewById(R.id.rg_extend_payment);
        rd_radio_payment2 = findViewById(R.id.rg_extend_payment2);
        /*sp_o = (Spinner) findViewById(R.id.sp_person);
        sp_t = (Spinner) findViewById(R.id.sp_day);
        sp_th = (Spinner) findViewById(R.id.sp_period);*/

        progressDialog = ProgressDialog.show(Vol_h_s.this, null, null, false, true);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.layout_loading_dialog);
        progressDialog.setCancelable(false);
        progressDialog.dismiss();

        /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(Need_h_s.this, android.R.layout.simple_spinner_dropdown_item, o);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_o.setAdapter(adapter);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(Need_h_s.this, android.R.layout.simple_spinner_dropdown_item, t);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_t.setAdapter(adapter2);

        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(Need_h_s.this, android.R.layout.simple_spinner_dropdown_item, th);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_th.setAdapter(adapter3);*/

        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String helthissue = HealthIssue.getText().toString();
                /*String spring_s1 = (sp_o.getSelectedItem().toString());
                String spring_s2 = (sp_t.getSelectedItem().toString());
                String spring_s3 = (sp_th.getSelectedItem().toString());*/

                if (!helthissue.isEmpty()) {
                    try {

                        RequestBody CategoryOfHelp = RequestBody.create(MediaType.parse("text/plain"), "Health");
                        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), Vol_H.name_s);
                        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), Vol_H.phone_s);
                        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), Vol_H.email_s);
                        RequestBody pin = RequestBody.create(MediaType.parse("text/plain"), Vol_H.pin_s);
                        RequestBody add = RequestBody.create(MediaType.parse("text/plain"), Vol_H.add_s);
                        RequestBody woard = RequestBody.create(MediaType.parse("text/plain"), Vol_H.spring_s);

                        RequestBody spring__s1 = RequestBody.create(MediaType.parse("text/plain"), "N/A");
                        RequestBody spring__s2 = RequestBody.create(MediaType.parse("text/plain"), "N/A");
                        RequestBody spring__s3 = RequestBody.create(MediaType.parse("text/plain"), "N/A");
                        RequestBody spring__s4 = RequestBody.create(MediaType.parse("text/plain"), "N/A");
                        RequestBody spring__s5 = RequestBody.create(MediaType.parse("text/plain"), "N/A");
                        RequestBody helth_issue = RequestBody.create(MediaType.parse("text/plain"), helthissue);
                        int selectedId = rd_radio_payment.getCheckedRadioButtonId();
                        RadioButton radioSexButton = (RadioButton) findViewById(selectedId);
                        String mode = String.valueOf(radioSexButton.getText());
                        mode__S1 = RequestBody.create(MediaType.parse("text/plain"), mode);

                        int selectedId2 = rd_radio_payment2.getCheckedRadioButtonId();
                        RadioButton radioSexButton2 = (RadioButton) findViewById(selectedId2);
                        String mode2 = String.valueOf(radioSexButton2.getText());
                        mode__S2 = RequestBody.create(MediaType.parse("text/plain"), mode2);

                        progressDialog.show();

                        Url.getWebService().sevaDevaRegistration(CategoryOfHelp, name, phone, email, woard, pin, add, spring__s4, helth_issue, spring__s5, spring__s1, spring__s2, spring__s3, helth_issue, mode__S1, mode__S2).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                progressDialog.dismiss();
                                if (response.isSuccessful()) {
                                    try {
                                        JSONObject responseObject = AppConstant.getResponseObject(response);

                                        if (responseObject.optBoolean("IsSuccess")) {
                                            String detail = responseObject.optString("ResponseData");
                                            try {

                                                AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(Vol_h_s.this);
                                                myAlertDialog.setMessage("Your request has been send successfully");
                                                myAlertDialog.setPositiveButton("ok",
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface arg0, int arg1) {
                                                                Intent intent = new Intent(Vol_h_s.this, Selection.class);
                                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                startActivity(intent);
                                                            }
                                                        });
                                                myAlertDialog.setCancelable(false);
                                                myAlertDialog.show();

                                            } catch (Exception e) {

                                            }
                                        } else {
                                            Toast.makeText(Vol_h_s.this, responseObject.optString("Message"), Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    Toast.makeText(Vol_h_s.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                progressDialog.dismiss();
                                Toast.makeText(Vol_h_s.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                                Log.d("", "Error in Ticket Category : " + t.getMessage());
                            }
                        });
                    } catch (Exception e) {

                        try {
                            progressDialog.dismiss();
                        } catch (Exception e1) {

                        }

                        Toast.makeText(Vol_h_s.this, R.string.error_amc, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    HealthIssue.setError("Invalid");
                }
            }
        });
    }
}